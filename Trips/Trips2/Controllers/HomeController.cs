﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Trips.Controllers
{
    public class HomeController : Controller
    {
        // proovi muudatus
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Reisikulude arvestamine";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Trips Meeskonna kontaktid";

            return View();
        }
    }
}