﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Trips.Models
{
    public class Person
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Nimi ei tohi olla tühi")]
        [Display(Name = "Eesnimi")]

        public string FirstName { get; set; }
        [Required(ErrorMessage = "Nimi ei tohi olla tühi")]
        [Display(Name = "Perekonnanimi")]

        public string LastName { get; set; }
        [Required(ErrorMessage = "Nimi ei tohi olla tühi")]
        [Display(Name = "Osakond")]

        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
       
        [Required(ErrorMessage = "Nimi ei tohi olla tühi")]
        [Display(Name = "Roll")]
       
        public int? RoleId { get; set; }
        public Role Role { get; set; }

        public ICollection<Person> person { get; set; }





    }
}