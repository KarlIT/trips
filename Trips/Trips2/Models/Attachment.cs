﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trips.Models
{
    public class Attachment
    {
            public string PersonId { get; set; }
            public int AttachmentId { get; set; }
            public string Title { get; set; }
            public DateTime ReleaseDate { get; set; }
            public decimal Sum { get; set; }
    }
}