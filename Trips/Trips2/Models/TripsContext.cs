﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Trips.Models
{
    public class TripsContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public TripsContext() : base("name=TripsContext")
        {
        }

        public System.Data.Entity.DbSet<Trips.Models.Person> People { get; set; }

        public System.Data.Entity.DbSet<Trips.Models.Trip> Trips { get; set; }

        public System.Data.Entity.DbSet<Trips.Models.Department> Departments { get; set; }

        public System.Data.Entity.DbSet<Trips.Models.Role> Roles { get; set; }
    }
}
