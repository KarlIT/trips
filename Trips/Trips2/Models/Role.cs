﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trips.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Person> People { get; set; }
    }
}