﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Trips.Models
{
    public class Trip
    {
        public int Id { get; set; }

        [Display(Name = "Sihtkoht")]
        public string Destination { get; set; }

        [Display(Name = "Alguskuupäev")]
        [DataType(DataType.Date)]
        public DateTime DepartureDate { get; set; }

        [Display(Name = "Lõppkuupäev")]
        [DataType(DataType.Date)]
        public DateTime ArrivalDate { get; set; }
        //
        [Display(Name = "Majutuskulud")]
        public double HousingExpenses { get; set; }


        [Display(Name = "Transpordikulud")]
        public double TransportExpenses { get; set; }

        [Display(Name = "Muud kulud")]
        public double OtherExpenses { get; set; }

        public byte[] Picture { get; set; }

        public bool isSubmitted { get; set; }


        public int? PersonId { get; set; }

        
        //iga reisi küljes on person: vt.person.cs
        public Person Person { get; set; }












    }
}