﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Trips.Startup))]
namespace Trips
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
