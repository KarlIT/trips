﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trips.Models
{
    public class Document
    {
        public int Id { set; get; }
        public string FileName { get; set; }
        public byte[] File { get; set; }

        public ICollection<ApplicationUser> ApplicationUsers { get; set; }

    }
}