﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Trips.Models
{
    public class ActionLog
    {
        public int Id { get; set; }
        
        [Display(Name = "Aeg")]
        public DateTime ActionTime { get; set; }
       
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        [Display(Name = "Tegevus")]
        public string Action { get; set; }                
    }
}