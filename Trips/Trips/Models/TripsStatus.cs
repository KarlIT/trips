﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Trips.Models;

namespace Trips.Models
{
    public class TripStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Trip> Trips { get; set; }
    }
}