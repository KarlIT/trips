﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trips.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection <ApplicationUser> ApplicationUsers { get; set; }
    }
}