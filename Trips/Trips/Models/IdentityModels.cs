﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Collections;

namespace Trips.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Nimi ei tohi olla tühi")]

        [Display(Name = "Perekonnanimi")]
                public string LastName { get; set; }
        [Required(ErrorMessage = "Nimi ei tohi olla tühi")]
        [Display(Name = "Osakond")]

        public int? DepartmentId { get; set; }
        public Department Department { get; set; }

        public ICollection<Trip> Trips { get; set; }

        public ICollection<ActionLog> ActionLogs { get; set; }
    }

    public class TripsContext : IdentityDbContext<ApplicationUser>
    {
        public TripsContext()
            : base("TripsContext", throwIfV1Schema: false)
        {
        }

        public static TripsContext Create()
        {
            return new TripsContext();
        }


        public System.Data.Entity.DbSet<Trips.Models.Trip> Trips { get; set; }

        public System.Data.Entity.DbSet<Trips.Models.Department> Departments { get; set; }

        public System.Data.Entity.DbSet<Trips.Models.TripStatus> TripStatuses { get; set; }

        public System.Data.Entity.DbSet<Trips.Models.Document> Documents { get; set; }

        public System.Data.Entity.DbSet<Trips.Models.ActionLog> ActionLogs { get; set; }

    }
}