﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Trips.Models
{
    public class Trip
    {
        public int Id { get; set; }

        [Display(Name = "Sihtkoht")]
        public string Destination { get; set; }

        [Display(Name = "Alguskuupäev")]
        [DataType(DataType.Date)]
        public DateTime DepartureDate { get; set; }

        [Display(Name = "Lõppkuupäev")]
        [DataType(DataType.Date)]
        public DateTime ArrivalDate { get; set; }
        //
        [Display(Name = "Majutuskulud")]
        public double HousingExpenses { get; set; }


        [Display(Name = "Transpordikulud")]
        public double TransportExpenses { get; set; }

        [Display(Name = "Muud kulud")]
        public double OtherExpenses { get; set; }



        [Display(Name = "Ööbimiste arv")]
        [NotMapped]
        public double Overnights
        {
            get
            {
                return (ArrivalDate - DepartureDate).TotalDays;
            }
        }

        [Display(Name = "Päevarahad")]
        [NotMapped]
        public double DailyAllowances
        {
            get
            {
                return (((ArrivalDate - DepartureDate).TotalDays) + 1) * 10;
            }
        }
        [Display(Name = "Sisestatud dokumendid")]
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string UserId { get; set; }
        public int? TripStatusId { get; set; }
        public TripStatus TripStatus { get; set; }

        //iga reisi küljes on person: vt.person.cs
        public ApplicationUser User { get; set; }


         
    }
}