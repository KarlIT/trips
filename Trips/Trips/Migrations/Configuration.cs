namespace Trips.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Trips.Models.TripsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Trips.Models.TripsContext context)
        {
            //see k�ivitatakse siis kui on esimene k�ivitamine, saame siia panna algandmed !!!!!!!!!!!!!!!!


            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //

            //N�ITEKS:
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            //context.Roles.AddOrUpdate(x => x.Name,
            //    new IdentityRole("T��taja esitamata"),
            //    new IdentityRole("Juhataja kinnitamata"),
            //    new IdentityRole("Raamatupidaja kinnitamata"),
            //    new IdentityRole("Raamatupidaja kinnitatud");
        



            //N�ITEKS: !!!!!!!!

            //var adminRole = new IdentityRole("Admin");
            //context.Roles.AddOrUpdate(x => x.Name, adminRole);

            //var adminRole = new IdentityRole("Admin");
            //context.Roles.AddOrUpdate(x => x.Name, 
            //   new IdentityRole("Admin"),
            //   new IdentityRole("User"),
            //   new IdentityRole("Moderator")
            //);

        }
    }
}
