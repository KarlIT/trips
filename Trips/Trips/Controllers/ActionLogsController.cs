﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Trips.Models;

namespace Trips.Controllers
{
    public class ActionLogsController : Controller
    {
        private TripsContext db = new TripsContext();

        // GET: ActionLogs
        public ActionResult Index()
        {
            var actionLogs = db.ActionLogs.Include(a => a.User);
            return View(actionLogs.ToList());
        }

        // GET: ActionLogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionLog actionLog = db.ActionLogs.Find(id);
            if (actionLog == null)
            {
                return HttpNotFound();
            }
            return View(actionLog);
        }

        // GET: ActionLogs/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName");
            return View();
        }

        // POST: ActionLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ActionTime,UserId,Action")] ActionLog actionLog)
        {
            if (ModelState.IsValid)
            {
                db.ActionLogs.Add(actionLog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", actionLog.UserId);
            return View(actionLog);
        }

        // GET: ActionLogs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionLog actionLog = db.ActionLogs.Find(id);
            if (actionLog == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", actionLog.UserId);
            return View(actionLog);
        }

        // POST: ActionLogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ActionTime,UserId,Action")] ActionLog actionLog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actionLog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", actionLog.UserId);
            return View(actionLog);
        }

        // GET: ActionLogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionLog actionLog = db.ActionLogs.Find(id);
            if (actionLog == null)
            {
                return HttpNotFound();
            }
            return View(actionLog);
        }

        // POST: ActionLogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ActionLog actionLog = db.ActionLogs.Find(id);
            db.ActionLogs.Remove(actionLog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
