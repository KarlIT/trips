﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Trips.Models;
using PagedList;


namespace Trips.Controllers
{
    public class TripsController : Controller
    {
        private TripsContext db = new TripsContext();

        // GET: Trips
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "Name";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;


            IEnumerable<Trip> trips = db.Trips.Include("User").Include("TripStatus").Where(x => x.UserId != null );
            if (!String.IsNullOrEmpty(searchString))
            {
                trips = trips.Where(s => s.User.LastName.Contains(searchString)
                                       || s.User.FirstName.Contains(searchString));
            }
            // sorteerimine, vaikimisi viimased reisid enne

            switch (sortOrder)
            {
                case "Name":
                    trips = trips.OrderBy(s => s.User.LastName);
                    break;
                case "name_desc":
                    trips = trips.OrderByDescending(s => s.User.LastName);
                    break;
                case "Date":
                    trips = trips.OrderBy(s => s.DepartureDate);
                    break;
                default:
                    trips = trips.OrderByDescending(s => s.DepartureDate);
                    break;
            }
            //viimase 6 kuu reisid

            var lowerlimit = DateTime.Now.AddMonths(-6);
            trips = trips.Where(a => a.DepartureDate > lowerlimit);

            //ühele lehele mahub 20 reisi
            int pageSize = 20 ;
            int pageNumber = (page ?? 1);
            

            var userId = User.Identity.GetUserId();
            var userName = User.Identity.GetUserName();
          

            if (User.IsInRole("Juhataja"))
            {
                var currentUser = db.Users.Find(userId); //see olen mina ise

                var currentDepartmentUserIds = db.Users.Where(a => a.DepartmentId == currentUser.DepartmentId).Select(a => a.Id); //kõik teised samast osakonnast

                trips = trips.Where(x => currentDepartmentUserIds.Contains(x.UserId));
            }

            else if(User.IsInRole("Töötaja"))
            {
                trips = trips.Where(x => x.UserId == userId);

            }
            

            return View(trips.ToPagedList(pageNumber, pageSize));


        }


        // GET: Trips/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Include("User").Where(x => x.Id == id).FirstOrDefault();
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // GET: Trips/ĆhangeStatus
        public ActionResult ChangeStatus(int tripId, int? tripStatusId)
        {
            if (tripStatusId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Trip trip = db.Trips.Find(tripId);

            trip.TripStatusId = (int)tripStatusId;
            db.Entry(trip).State = EntityState.Modified;

            var actionLog = new ActionLog()
            {
                Action = "Staatus muudeti",
                ActionTime = DateTime.Now,
                UserId = trip.UserId
            };

            db.ActionLogs.Add(actionLog);

            db.SaveChanges();

            //Trip trip = db.Trips.Include("User").Where(x => x.Id == id).FirstOrDefault();
            if (trip == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index");
        }


        // GET: Trips/Create
        public ActionResult Create()
        {
            var  person = db.Users.Find(User.Identity.GetUserId());

            ViewBag.PersonId = person.Id;            
            ViewBag.PersonName = person.FirstName + " " + person.LastName;

            return View();
        }

        // POST: Trips/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Destination,DepartureDate,ArrivalDate,HousingExpenses,TransportExpenses,OtherExpenses,UserId")] Trip trip)
        {
            
            //int result = DateTime.Compare(trip.ArrivalDate, trip.DepartureDate);

            //if (result < 0)
            //{
            //    trip.ArrivalDate = trip.DepartureDate;
            //}

            if (trip.DepartureDate >= trip.ArrivalDate)
            {
                return Content("Alguskuupäev ei saa olla hilisem kui lõpukuupäev");

            }
            

            if (ModelState.IsValid)
            {
                trip.TripStatusId = 1;
                db.Trips.Add(trip);
               

                var actionLog = new ActionLog()
                {
                    Action = "Trip lisati",
                    ActionTime = DateTime.Now,
                    UserId = trip.UserId
                };

                db.ActionLogs.Add(actionLog);

                db.SaveChanges();
                return RedirectToAction("Index");
            }           
            
            return View(trip);
        }

        // GET: Trips/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Include("User").Include("TripStatus").Where(x => x.Id == id).FirstOrDefault();
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // POST: Trips/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Destination,DepartureDate,ArrivalDate,HousingExpenses,TransportExpenses,OtherExpenses,UserId, TripStatusId, DailyAllowances")] Trip trip)
        {
            int result = DateTime.Compare(trip.ArrivalDate, trip.DepartureDate);
     
            if (result < 0)
            {
                trip.ArrivalDate = trip.DepartureDate;
            }

            if (ModelState.IsValid)
            {
                
                db.Entry(trip).State = EntityState.Modified;

                var actionLog = new ActionLog()
                {
                    Action = "Trip muudeti",
                    ActionTime = DateTime.Now,
                    UserId = trip.UserId
                };

                db.ActionLogs.Add(actionLog);

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trip);
        }

        // GET: Trips/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // POST: Trips/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Trip trip = db.Trips.Find(id);
            db.Trips.Remove(trip);

            var actionLog = new ActionLog()
            {
                Action = "Trip kustutati",
                ActionTime = DateTime.Now,
                UserId = trip.UserId
            };

            db.ActionLogs.Add(actionLog);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult FileUpload(HttpPostedFileBase file, int id)
        {
            Trip trip = db.Trips.Find(id);
            if (file != null)
            {
                                // save the image path path to the database or you can send image
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                    trip.File = array;
                    trip.FileName = file.FileName;

                    Path.GetFileName(file.FileName);
                    trip.FileName = Path.GetFileName(file.FileName);

                    db.Entry(trip).State = EntityState.Modified;

                    var actionLog = new ActionLog()
                    {
                        Action = "Fail lisati",
                        ActionTime = DateTime.Now,
                        UserId = trip.UserId
                    };

                    db.ActionLogs.Add(actionLog);

                    db.SaveChanges();

                    return RedirectToAction("index");
                }

            }
            // after successfully uploading redirect the user
            return Content("Unustasid faili valida");
            
        }

        public ActionResult Show(int id)
        {
            var trip = db.Trips.Find(id);

            if (trip.File == null)
            {
                return Content("Pilti pole");
            }


            return File(trip.File, "image");
        }
        public ActionResult Download(int id)
        {
            var trip = db.Trips.Find(id);

            if (trip.File == null)
            {
                return Content("Fail puudub");
            }


            return File(trip.File, "application/octet-stream", trip.FileName);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
